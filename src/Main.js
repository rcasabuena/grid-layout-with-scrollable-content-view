import React from 'react';
import Frame from './components/Frame';
import Grid from './components/Grid';

const Main = () => (
  <>
    <Frame />
    <Grid />
  </>
);

export default Main;