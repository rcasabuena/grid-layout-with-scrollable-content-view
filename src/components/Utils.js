export const calcWinsize = () => ({width: window.innerWidth, height: window.innerHeight});
export const getOffset = (elem, axis) => {
  let offset = 0;
  const type = axis === 'top' ? 'offsetTop' : 'offsetLeft';
  do {
    if ( !isNaN( elem[type] ) )
    {
      offset += elem[type];
    }
  } while( elem = elem.offsetParent );
  return offset;
}
// Calculates the distance between two points.
export const distance = (p1,p2) => Math.hypot(p2.x-p1.x, p2.y-p1.y);
// Generates a random number.
export const randNumber = (min,max) => Math.floor(Math.random() * (max - min + 1)) + min;
// Gets the mouse position. From http://www.quirksmode.org/js/events_properties.html#position
export const getMousePos = (e) => {
  let posx = 0;
  let posy = 0;
  if (!e) e = window.event;
  if (e.pageX || e.pageY) 	{
    posx = e.pageX;
    posy = e.pageY;
  }
  else if (e.clientX || e.clientY) 	{
    posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
    posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
  }
  return { x : posx, y : posy }
};
// Returns the rotation angle of an element.
export const getAngle = (el) => {
  const st = window.getComputedStyle(el, null);
  const tr = st.getPropertyValue('transform');
  let values = tr.split('(')[1];
  values = values.split(')')[0];
  values = values.split(',');
  return Math.round(Math.asin(values[1]) * (180/Math.PI));
};
// Scroll control functions. Taken from https://stackoverflow.com/a/4770179.
const keys = {37: 1, 38: 1, 39: 1, 40: 1};
export const preventDefault = (e) => {
  e = e || window.event;
  if (e.preventDefault)
    e.preventDefault();
  e.returnValue = false;  
}
export const preventDefaultForScrollKeys = (e) => {
  if (keys[e.keyCode]) {
    preventDefault(e);
    return false;
  }
}
export const disableScroll = () => {
  if (window.addEventListener) // older FF
    window.addEventListener('DOMMouseScroll', preventDefault, false);
  window.onwheel = preventDefault; // modern standard
  window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
  window.ontouchmove  = preventDefault; // mobile
  document.onkeydown  = preventDefaultForScrollKeys;
}
export const enableScroll = () => {
  if (window.removeEventListener)
    window.removeEventListener('DOMMouseScroll', preventDefault, false);
  window.onmousewheel = document.onmousewheel = null; 
  window.onwheel = null; 
  window.ontouchmove = null;  
  document.onkeydown = null;  
}