import React, { Component } from 'react';
import charming from 'charming';
import { TweenMax, Back, Quart } from 'gsap';

class ContentItem extends Component {

  constructor(props) {
    super(props);
    this.el = null;
    // The content elements: image, title, subtitle and text.
    this.img = null;
    this.title = null;
    this.subtitle = null;
    this.text = null;
    // And access the spans/letters.
    this.titleLetters = null;
    this.titleLettersTotal = 0;
  }

  componentDidMount() {
    this.img = this.el.querySelector('.content__item-img');
    this.title = this.el.querySelector('.content__item-title');
    this.subtitle = this.el.querySelector('.content__item-subtitle');
    this.text = this.el.querySelector('.content__item-text');

    // Split the title into spans using charming.js
    charming(this.title);
    this.titleLetters = this.title.querySelectorAll('span');
    this.titleLettersTotal = this.titleLetters.length;
  }

  show(delay = 0, withAnimation = true) { this.toggle(delay, withAnimation); }
  hide(delay = 0, withAnimation = true) { this.toggle(delay, withAnimation, false); }
  toggle(delay, withAnimation, show = true) {
    setTimeout(() => {
      this.titleLetters.forEach((letter,pos) => {
        TweenMax.to(letter, !withAnimation ? 0 : show ? .6 : .3, {
          ease: show ? Back.easeOut : Quart.easeIn,
          delay: !withAnimation ? 0 : show ? pos*.05 : (this.titleLettersTotal-pos-1)*.04,
          startAt: show ? {y: '50%', opacity: 0} : null,
          y: show ? '0%' : '50%',
          opacity: show ? 1 : 0
        });
      });
      this.subtitle.style.opacity = show ? 1 : 0;
      this.text.style.opacity = show ? 1 : 0;

    }, withAnimation ? delay*1000 : 0 );
  }

  render() {
    const { title, subtitle, text, src, alt } = this.props;
    return (
      <div className="content__item" ref={el => this.el = el}>
        <div className="content__item-intro">
          <img className="content__item-img" src={src} alt={alt} />
          <h2 className="content__item-title">{title}</h2>
        </div>
        <h3 className="content__item-subtitle">{subtitle}</h3>
        <div className="content__item-text" dangerouslySetInnerHTML={{__html: text}}></div>
      </div>
    );
  };
};

export default ContentItem;
