export const SET_CONTENT = 'SET_CONTENT';
export const setContent = content => ({
  type: SET_CONTENT,
  payload: content,
});

export const UNSET_CONTENT = 'UNSET_CONTENT';
export const unsetContent = () => ({
  type: UNSET_CONTENT,
  payload: null,
});
