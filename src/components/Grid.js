import React, { Component } from 'react';
import { connect } from 'react-redux';
import imagesLoaded from 'imagesloaded';
import Masonry from 'masonry-layout';
import GridItemComponent from './GridItem';
import ContentItemComponent from './ContentItem';
import { getOffset, disableScroll, enableScroll, calcWinsize, distance } from './Utils';
import { TweenMax, Expo } from 'gsap';

class Grid extends Component {

  constructor(props) {
    super(props);
    this.el = null;
    this.allowTilt = true;
    this.gridWrap = null; // The grid wrap.
    this.items = []; // The grid items.
    this.itemsTotal = 0; // The total number of items.
    this.contents = []; // The content items.
    // Back control and scroll indicator (elements shown when the item´s content is open).
		this.closeCtrl = null;
		this.scrollIndicator = null;
		// The open grid item.
    this.current = -1;
    this.winsize = {};
  }

  componentDidMount() {

    this.winsize = calcWinsize();
    this.gridWrap = this.el.parentNode;
    this.itemsTotal = this.items.length;
    this.closeCtrl = document.querySelector('.content__close');
		this.scrollIndicator = document.querySelector('.content__indicator');
    
    imagesLoaded(document.querySelectorAll('.grid__item-img'), () => {
      document.body.classList.remove('loading');
      new Masonry( this.el, {
        // options
        itemSelector: '.grid__item',
        columnWidth: 260,
        gutter: 100,
        fitWidth: true
      });
      //this.initEvents();
    });
  }

  initEvents() {
    // Clicking a grid item hides all the other grid items (ordered by proximity to the clicked one) 
    // and expands/opens the clicked one.
    for (let item of this.items) {
      item.el.addEventListener('click', (ev) => {
        ev.preventDefault();
        this.openItem(item);
      });
    }
    // Close item.
    this.closeCtrl.addEventListener('click', () => this.closeItem());
    // (Incomplete! For now: if theres an open item, then show back the grid.
    this.resizeFn = () => {
      if (this.current === -1 || this.winsize.width === window.innerWidth) return;
      this.closeItem(false);
    };
    window.addEventListener('resize', this.resizeFn);
    window.addEventListener('resize', () => this.winsize = calcWinsize());
  }
  getSizePosition(el, scrolls = true) {
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
      const scrollLeft = window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft;
    return {
      width: el.offsetWidth,
      height: el.offsetHeight,
      left: scrolls ? getOffset(el, 'left')-scrollLeft : getOffset(el, 'left'),
      top: scrolls ? getOffset(el, 'top')-scrollTop : getOffset(el, 'top')
    };
  }
  openItem(item) {
    if ( this.isAnimating ) return;
    this.isAnimating = true;
    // Get the current scroll position.
    this.scrollPos = window.scrollY;
    // Disable page scrolling.
    disableScroll();
    // Disable tilt.
    this.allowTilt = false;
    // Set the current value (index of the clicked item).
    this.current = this.items.indexOf(item);
    // Hide all the grid items except the one we want to open.
    this.hideAllItems(item);
    // Also hide the item texts.
    item.hideTexts();
    // Set the item´s z-index to a high value so it overlaps any other grid item.
    item.el.style.zIndex = 1000;
    // Get the "grid__item-bg" width and height and set it explicitly, 
    // also set its top and left respective to the page.
    const itemDim = this.getSizePosition(item.DOM.el);
    item.DOM.bg.style.width = `${itemDim.width}px`;
    item.DOM.bg.style.height = `${itemDim.height}px`;
    item.DOM.bg.style.left = `${itemDim.left}px`;
    item.DOM.bg.style.top = `${itemDim.top}px`;
    // Set it to position fixed.
    item.DOM.bg.style.position = 'fixed';
    // Calculate the viewport diagonal. We will need to take this in consideration when scaling up the item´s bg element.
    const d = Math.hypot(this.winsize.width, this.winsize.height);
    // Scale up the item´s bg element.
    TweenMax.to(item.DOM.bg, 1.2, {
      ease: Expo.easeInOut,
      delay: 0.4,
      x: this.winsize.width/2 - (itemDim.left+itemDim.width/2),
      y: this.winsize.height/2 - (itemDim.top+itemDim.height/2),
      scaleX: d/itemDim.width,
      scaleY: d/itemDim.height,
      rotation: -1*item.angle*2
    });
    // Get the content element respective to this grid item.
    const contentEl = this.contents[this.current];
    // Set it to current.
    contentEl.el.classList.add('content__item--current');
    // Calculate the item´s image and content´s image sizes and positions. 
    // We need this so we can scale up and translate the item´s image to the same size and position of the content´s image.
    const imgDim = this.getSizePosition(item.DOM.imgWrap);
    const contentImgDim = this.getSizePosition(contentEl.DOM.img, false);
    // Show the back control and scroll indicator and all the item´s content elements (1 second delay).
    this.showContentElems(contentEl, 1);
    // Animate the item´s image.
    TweenMax.to(item.DOM.tilt.img, 1.2, {
      ease: Expo.easeInOut,
      delay: 0.55,
      scaleX: contentImgDim.width/imgDim.width,
      scaleY: contentImgDim.height/imgDim.height,
      x: (contentImgDim.left+contentImgDim.width/2)-(imgDim.left+imgDim.width/2),
      y: (contentImgDim.top+contentImgDim.height/2)-(imgDim.top+imgDim.height/2),
      rotation: 0,
      onComplete: () => {
        // Hide the item´s image and show the content´s image. Should both be overlapping.
        item.DOM.tilt.img.style.opacity = 0;
        contentEl.DOM.img.style.visibility = 'visible';
        // Set the main content wrapper to absolute so it´s position at the top.
        contentEl.DOM.el.parentNode.style.position = 'absolute';
        // Hiding the grid scroll.
        this.DOM.gridWrap.classList.add('grid-wrap--hidden');
        // Scroll up the page.
        window.scrollTo(0, 0);
        // Enable page scrolling.
        enableScroll();
        this.isAnimating = false;
      }
    });
  }
  closeItem(withAnimation = true) {
    if ( this.isAnimating ) return;
    this.isAnimating = true;
    // Get the content element respective to this grid item.
    const contentEl = this.contents[this.current];
    // Scroll to the previous scroll position before opening the item.
    window.scrollTo(0, this.scrollPos);
    contentEl.DOM.el.parentNode.style.position = 'fixed';
    // Disable page scrolling.
    disableScroll();
    // Showing the grid scroll.
    this.DOM.gridWrap.classList.remove('grid-wrap--hidden');
    // The item that is open.
    const item = this.items[this.current];
    // Hide the back control and scroll indicator and all the item´s content elements.
    this.hideContentElems(contentEl, 0, withAnimation);
    // Set the grid´s image back to visible and hide the content´s one.
    item.DOM.tilt.img.style.opacity = 1;
    contentEl.DOM.img.style.visibility = 'hidden';
    // Animate the grid´s image back to the grid position.
    TweenMax.to(item.DOM.tilt.img, withAnimation ? 1.2 : 0, {
      ease: Expo.easeInOut,
      scaleX: 1,
      scaleY: 1,
      x: 0,
      y: 0,
      rotation: item.angle*2
    });
    // And also the bg element.
    TweenMax.to(item.DOM.bg, withAnimation ? 1.2 : 0, {
      ease: Expo.easeInOut,
      delay: 0.15,
      x: 0,
      y: 0,
      scaleX: 1,
      scaleY: 1,
      rotation: 0,
      onComplete: () => {
        contentEl.DOM.el.classList.remove('content__item--current');
        item.DOM.bg.style.position = 'absolute';
        item.DOM.bg.style.left = '0px';
        item.DOM.bg.style.top = '0px';
        this.current = -1;
        this.allowTilt = true;
        item.DOM.el.style.zIndex = 0;
        enableScroll();
        this.isAnimating = false;
      }
    });
    // Show all the grid items except the one we want to close.
    this.showAllItems(item, withAnimation);
    // Also show the item texts. (1s delay)
    item.showTexts(1, withAnimation);
  }
  /**
   * Toggle the content elements.
   */
  showContentElems(contentEl, delay = 0, withAnimation = true) { this.toggleContentElems(contentEl, delay, withAnimation); }
  hideContentElems(contentEl, delay = 0, withAnimation = true) { this.toggleContentElems(contentEl, delay, withAnimation, false); }
  toggleContentElems(contentEl, delay, withAnimation, show = true) {
    // toggle the back control and scroll indicator.
    TweenMax.to([this.DOM.closeCtrl, this.DOM.scrollIndicator], withAnimation ? 0.8 : 0, {
      ease: show ? Expo.easeOut : Expo.easeIn,
      delay: withAnimation ? delay : 0,
      startAt: show ? {y: 60} : null,
      y: show ? 0 : 60,
      opacity: show ? 1 : 0
    });
    if ( show ) {
      contentEl.show(delay, withAnimation);
    }
    else {
      contentEl.hide(delay, withAnimation);
    }
  }
  // Based on https://stackoverflow.com/q/25481717
  sortByDist(refPoint, itemsArray) {
    let distancePairs = [];
    let output = [];

    for(let i in itemsArray) {
      const rect = itemsArray[i].DOM.el.getBoundingClientRect();
      distancePairs.push([distance(refPoint,{x:rect.left+rect.width/2, y:rect.top+rect.height/2}), i]);
    }

    distancePairs.sort((a,b) => a[0]-b[0]);

    for(let p in distancePairs) {
      const pair = distancePairs[p];
      output.push(itemsArray[pair[1]]);
    }

    return output;
  }
  /**
   * Shows/Hides all the grid items except the "exclude" item.
   * The items will be sorted based on the distance to the exclude item.
   */
  showAllItems(exclude, withAnimation = true) { this.toggleAllItems(exclude, withAnimation); }
  hideAllItems(exclude, withAnimation = true) { this.toggleAllItems(exclude, withAnimation, false); }
  toggleAllItems(exclude, withAnimation, show = true) {
    if ( !withAnimation ) {
      this.items.filter(item => item != exclude).forEach((item, pos) => item[show ? 'show' : 'hide'](withAnimation));
    }
    else {
      const refrect = exclude.DOM.el.getBoundingClientRect(); 
      const refPoint = {
        x: refrect.left+refrect.width/2, 
        y: refrect.top+refrect.height/2
      };
      this.sortByDist(refPoint, this.items.filter(item => item != exclude))
        .forEach((item, pos) => setTimeout(() => item[show ? 'show' : 'hide'](), show ? 300+pos*70 : pos*70));
    }
  }

  render() {
    const { items, content } = this.props;
    return (
      <>
        <div className="grid-wrap">
          <div className="grid" ref={el => this.el = el}>
            {items.map(item => <GridItemComponent key={item.number} {...item} />)}
          </div>
        </div>
        <div className="content">
          {items.map(item => <ContentItemComponent key={item.number} {...item} />)}
          <button className="content__close">Close</button>
          <svg className="content__indicator icon icon--caret"><use xlinkHref="#icon-caret"></use></svg>
        </div>
      </>
    );
  };
};

const mapStateToProps = state => ({
  items: state.items,
  content: state.content
});

const mapDispatchToProps = dispatch => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
