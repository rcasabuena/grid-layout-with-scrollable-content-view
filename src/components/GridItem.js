import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setContent } from './actions/content';
import charming from 'charming';
import { TweenMax, Expo, Quad, Elastic, Quart } from 'gsap';
import { getMousePos, getAngle, getOffset, calcWinsize, disableScroll } from './Utils';

class GridItem extends Component {
  constructor(props){ 
    super(props);
    this.el = null;
    this.bg = null; // The rectangle element around the image.
    this.allowTilt = true; // The following DOM elements are elements that will move/tilt when hovering the item.
    this.tilt = {};
    this.imgWrap = null; 
    this.tilt.img = null; // The image.
    this.tilt.title = null; // The title (vertical text).
    this.tilt.number = null; // The number (horizontal letter/number code).
    this.numberLetters = null; // And access the spans/letters.
    // Configuration for when moving/tilting the elements on hover.
    this.tiltconfig = {   
      title: {translation : {x: [-8,8], y: [4,-4]}},
      number: {translation : {x: [-5,5], y: [-10,10]}},
      img: {translation : {x: [-15,15], y: [-10,10]}}
    };
  }

  componentDidMount() {
    this.winsize = calcWinsize();
    this.bg = this.el.querySelector('.grid__item-bg');
    this.imgWrap = this.el.querySelector('.grid__item-wrap');
    this.tilt.img = this.imgWrap.querySelector('img');
    this.tilt.title = this.el.querySelector('.grid__item-title');
    this.tilt.number = this.el.querySelector('.grid__item-number');
    charming(this.tilt.number);
    this.numberLetters = this.tilt.number.querySelectorAll('span');
    // Get the rotation angle value of the image element.
    // This will be used to rotate the DOM.bg to the same value when expanding/opening the item.
    this.angle = getAngle(this.tilt.img);
    // Init/Bind events.
    this.initEvents();
  }

  componentDidUpdate() {
    const { content, items, number } = this.props;
    if (content !== null && content.number != number) this.hide(true);
    else {
      if ( this.isAnimating ) return;
      this.isAnimating = true;
      // Get the current scroll position.
      this.scrollPos = window.scrollY;
      // Disable page scrolling.
      disableScroll();
      // Disable tilt.
      this.allowTilt = false;
      this.hideTexts();
      this.el.style.zIndex = 1000;
      const itemDim = this.getSizePosition(this.el);
      this.bg.style.width = `${itemDim.width}px`;
      this.bg.style.height = `${itemDim.height}px`;
      this.bg.style.left = `${itemDim.left}px`;
      this.bg.style.top = `${itemDim.top}px`;
      // Set it to position fixed.
      this.bg.style.position = 'fixed';

      const d = Math.hypot(this.winsize.width, this.winsize.height);
      // Scale up the item´s bg element.
      TweenMax.to(this.bg, 1.2, {
        ease: Expo.easeInOut,
        delay: 0.4,
        x: this.winsize.width/2 - (itemDim.left+itemDim.width/2),
        y: this.winsize.height/2 - (itemDim.top+itemDim.height/2),
        scaleX: d/itemDim.width,
        scaleY: d/itemDim.height,
        rotation: -1*this.angle*2
      });
    }
  }

  getSizePosition(el, scrolls = true) {
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
      const scrollLeft = window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft;
    return {
      width: el.offsetWidth,
      height: el.offsetHeight,
      left: scrolls ? getOffset(el, 'left')-scrollLeft : getOffset(el, 'left'),
      top: scrolls ? getOffset(el, 'top')-scrollTop : getOffset(el, 'top')
    };
  }

  initEvents() {
    this.toggleAnimationOnHover = (type) => {
      // Scale up the bg element.
      TweenMax.to(this.bg, 1, {
        ease: Expo.easeOut,
        scale: type === 'mouseenter' ? 1.15 : 1
      });
      // Animate the number letters.
      this.numberLetters.forEach((letter,pos) => {
        TweenMax.to(letter, .2, {
          ease: Quad.easeIn,
          delay: pos*.1,
          y: type === 'mouseenter' ? '-50%' : '50%',
          opacity: 0,
          onComplete: () => {
            TweenMax.to(letter, type === 'mouseenter' ? 0.6 : 1, {
              ease: type === 'mouseenter' ? Expo.easeOut : Elastic.easeOut.config(1,0.4),
              startAt: {y: type === 'mouseenter' ? '70%' : '-70%', opacity: 0},
              y: '0%',
              opacity: 1
            });
          }
        });
      });
    };
    this.mouseenterFn = (ev) => {
      if ( !this.allowTilt ) return;
      this.toggleAnimationOnHover(ev.type);
    };
    
    this.mousemoveFn = (ev) => requestAnimationFrame(() => {
      if ( !this.allowTilt ) return;
          this.tiltItem(ev);
    });
    this.mouseleaveFn = (ev) => {
      if ( !this.allowTilt ) return;
      this.resetTilt();
      this.toggleAnimationOnHover(ev.type);
    };
    this.clickFn = (ev) => {
      
    }
    this.el.addEventListener('mouseenter', this.mouseenterFn);
    this.el.addEventListener('mousemove', this.mousemoveFn);
    this.el.addEventListener('mouseleave', this.mouseleaveFn);
    this.el.addEventListener('click', this.clickFn);
  }
  tiltItem(ev) {
    const mousepos = getMousePos(ev); // Get mouse position.
    // Document scrolls
    const docScrolls = {left : document.body.scrollLeft + document.documentElement.scrollLeft, top : document.body.scrollTop + document.documentElement.scrollTop};
    const bounds = this.el.getBoundingClientRect();
    // Mouse position relative to the main element (this.el).
    const relmousepos = {
        x : mousepos.x - bounds.left - docScrolls.left, 
        y : mousepos.y - bounds.top - docScrolls.top 
    };
    // Movement settings for the tilt elements.
    for (let key in this.tilt) {
      let t = this.tiltconfig[key].translation;
      // Animate each of the elements..
      TweenMax.to(this.tilt[key], 2, {
          ease: Expo.easeOut,
          x: (t.x[1]-t.x[0])/bounds.width*relmousepos.x + t.x[0],
          y: (t.y[1]-t.y[0])/bounds.height*relmousepos.y + t.y[0]
      });
    }
  }
  resetTilt() {
    for (let key in this.tilt ) {
      TweenMax.to(this.tilt[key], 2, {
        ease: Elastic.easeOut.config(1,0.4),
        x: 0,
        y: 0
      });
    }
  }
  /**
   * Hides the item:
   * - Scales down and fades out the image and bg elements.
   * - Moves down and fades out the title and number elements.
   */
  hide(withAnimation = true) { this.toggle(withAnimation,false); }
  /**
   * Resets.
   */
  show(withAnimation = true) { this.toggle(withAnimation); }
  toggle(withAnimation, show = true) {
    TweenMax.to(this.tilt.img, withAnimation ? 0.8 : 0, {
      ease: Expo.easeInOut,
      delay: !withAnimation ? 0 : show ? 0.15 : 0,
      scale: show ? 1 : 0,
      opacity: show ? 1 : 0,
    });
    TweenMax.to(this.bg, withAnimation ? 0.8 : 0, {
      ease: Expo.easeInOut,
      delay: !withAnimation ? 0 : show ? 0 : 0.15,
      scale: show ? 1 : 0,
      opacity: show ? 1 : 0
    });
    this.toggleTexts(show ? 0.45 : 0, withAnimation, show);
  }
  // hides the texts (translate down and fade out).
  hideTexts(delay = 0, withAnimation = true) { this.toggleTexts(delay, withAnimation, false); }
  // shows the texts (reset transforms and fade in).
  showTexts(delay = 0, withAnimation = true) { this.toggleTexts(delay, withAnimation); }
  toggleTexts(delay, withAnimation, show = true) {
    TweenMax.to([this.tilt.title, this.tilt.number], !withAnimation ? 0 : show ? 1 : 0.5, {
      ease: show ? Expo.easeOut : Quart.easeIn,
      delay: !withAnimation ? 0 : delay,
      y: show ? 0 : 20,
      opacity: show ? 1 : 0
    });
  }
  
  render() {
    const { src, alt, title, number, onItemOpen } = this.props
    //if (content == null) this.hide(true);
    return (
      <a className="grid__item" 
        ref={el => this.el = el}
        onClick={
          () => {
            onItemOpen({src, alt, title, number})
          }
        }   
      >
        <div className="grid__item-bg"></div>
        <div className="grid__item-wrap">
          <img className="grid__item-img" src={src} alt={alt} />
        </div>
        <h3 className="grid__item-title">{title}</h3>
        <h4 className="grid__item-number">{number}</h4>
      </a>  
    );
  };
};

const mapStateToProps = state => {
  return {
    items: state.items,
    content: state.content,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onItemOpen: content => {
      dispatch(setContent(content))
    },
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(GridItem);
