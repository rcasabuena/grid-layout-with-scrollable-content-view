import React, { Component } from 'react';

class Frame extends Component {
  render() {
    return (
      <div className="frame">
        <header className="project-header">
          <h1 className="project-header__title">Grid Layout with Scrollable Content View</h1>
          <div className="project-links">
            <a className="bitbucket" href="https://bitbucket.org/rcasabuena/grid-layout-with-scrollable-content-view" title="Find this project on Bitbucket"><svg className="icon icon--bitbucket"><use xlinkHref="#icon-bitbucket"></use></svg></a>
          </div>
        </header>
        <div className="title">
          <h3 className="title__name">Julian Saber</h3>
          <h4 className="title__sub">2018 <span className="title__sub-works">Selected Works</span></h4>
        </div>
      </div>
    );
  };
};

export default Frame;
