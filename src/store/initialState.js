import data from './data';

const initialState = {
  items: data,
  content: null,
} 

export default initialState;