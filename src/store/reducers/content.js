const content = (state = {}, action) => {
  switch (action.type) {
    case 'SET_CONTENT':
      return {...action.payload};
    
    case 'UNSET_CONTENT':
      return null;
    
    default:
      return state;
  };
};

export default content;