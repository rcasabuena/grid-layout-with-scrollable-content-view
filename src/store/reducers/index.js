import { combineReducers } from 'redux';
import items from './items';
import content from './content';

export default combineReducers({
  items,
  content,
});