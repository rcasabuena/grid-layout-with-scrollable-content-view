import initialState from './initialState';
import reducers from './reducers';
import { createStore, applyMiddleware } from 'redux';

const consoleMessages = store => next => action => {
  let result;
	
	console.groupCollapsed(`dispatching action => ${action.type}`);
	result = next(action);
	console.groupEnd();
	
	return result;
}

export const configureStore = () => {
  return applyMiddleware(consoleMessages)(createStore)(reducers, initialState);
};
